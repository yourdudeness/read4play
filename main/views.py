from django.shortcuts import render
from django.views.generic import TemplateView

class ItemSite(TemplateView):
    template_name = 'index.html'

class RegisterUser(TemplateView):
    template_name = 'register.html'

class Privacy(TemplateView):
    template_name = 'term.html'

class News(TemplateView):
    template_name = "news.html"

class NewsList(TemplateView):
    template_name = 'news-list.html'

class Settings(TemplateView):
    template_name = 'settings.html'
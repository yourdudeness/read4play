from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.ItemSite.as_view(), name = 'index'),
    path('join', views.RegisterUser.as_view(), name = 'register'),
    path('privacy', views.Privacy.as_view(), name = 'privacy'),
    path('news', views.News.as_view(), name = 'news'),
    path('news-list', views.NewsList.as_view(), name = 'news-list'),
    path('settings', views.Settings.as_view(), name = 'settings'),
]
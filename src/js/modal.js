$(window).on('load', function () {
    $('#enter').on('click', function (e) {
        e.preventDefault();
        $("#overlay").show();
    });

    $('.info__header-close').on('click', function () {
        $("#entry").trigger('reset');
        $("#registr").trigger('reset');
        $("#overlay,#name-error, #pass-error, #login-error, #pass-reg-error,#pass-repeat-reg-error,#email-reg-error").hide();
    });

    // $('#allow').on('change', function () {
    //     let check = $(this).is(':checked');
    //     let btn = $(this).closest('.entry').find('.button__come');

    //     if (check) {
    //         btn.removeAttr('disabled');
    //     } else {
    //         btn.attr('disabled', true)
    //     }
    // });

    $('#allow-reg').on('change', function () {
        let check = $(this).is(':checked');
        let btn = $(this).closest('.registr').find('.button__come');

        if (check) {
            btn.removeAttr('disabled');
        } else {
            btn.attr('disabled', true)
        }
    });

    $('#tab-enter').on('click', function () {
        $("#registr").trigger('reset');
    });

    $('#tab-reg').on('click', function () {
        $("#entry").trigger('reset');
    });


});

let tab = document.querySelectorAll('.info__header-tab'),
    info = document.querySelector('.info__header'),
    tabContent = document.querySelectorAll('.info__tabcontent');

function hideTabContent(a) {
    for (let i = a; i < tabContent.length; i++) {
        tabContent[i].classList.remove('show');
        tabContent[i].classList.add('hide');

    }
}

hideTabContent(1);

function showTabContent(b) {
    if (tabContent[b].classList.contains('hide')) {
        tabContent[b].classList.remove('hide');
        tabContent[b].classList.add('show');

    }
}

info.addEventListener('click', function (event) {
    let target = event.target;
    $('.info__header-tab').removeClass('non-border');
    $(target).addClass('non-border');
    
    
    if (target && target.classList.contains('info__header-tab')) {
        for (let i = 0; i < tab.length; i++) {
            if (target == tab[i]) {
                hideTabContent(0);
                showTabContent(i);
                break;
            }
        }
    }

});
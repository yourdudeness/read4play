$(window).on('load', function () {
    $(document).click(function (event) {
        if ($(event.target).closest('.profile-nav__submenu').length)
            return;
        $('.profile-nav__submenu').hide();
        event.stopPropagation();
    });
    $('.profile-nav__dropdown').click(function () {

        if ($(this).siblings('.profile-nav__submenu').is(':visible')) {
            $(this).siblings('.profile-nav__submenu').hide();
        } else {
            $(this).siblings('.profile-nav__submenu').show();
        }
        return false;
    });

    $('#entry').validate({
        rules: {
            username: {
                required: true,

            },
            password: {
                required: true,

            },
        },
        messages: {
            username: {
                required: 'Поле обязательно к заполнению',
            },
            password: {
                required: 'Поле обязательно к заполнению',
            },
        }
    });

    $('#registr').validate({
        rules: {
            login: {
                required: true,

            },
            password: {
                required: true,

            },
            password_sec: {
                required: true,
                equalTo: "#pass-reg",

            },
            email: {
                required: true,
                email: true,

            },
        },
        messages: {
            login: {
                required: 'Поле обязательно к заполнению',
            },
            password: {
                required: 'Поле обязательно к заполнению',
            },
            password_sec: {
                required: 'Поле обязательно к заполнению',
                equalTo: 'Пароли не совпадают',
            },
            email: {
                required: 'Поле обязательно к заполнению',
                email: 'Некорректный email',
            },
        },
        submitHandler: function () {
            if ($('#registr').valid()) {
                $('#overlay').hide();
                $("#entry").trigger('reset');
                $("#registr").trigger('reset');
                $.fancybox.open({
                    src: '#checkmail',
                    type: 'inline',
                    opts: {
                        afterLoad: function () {
                            var fancyboxSlide = document.querySelectorAll('.fancybox-slide');
                            fancyboxSlide.forEach(function (element) {
                                bodyScrollLock.disableBodyScroll(element);
                            });
                        },
                        beforeClose: function () {
                            if ($('.fancybox-slide').length == 1) {
                                bodyScrollLock.clearAllBodyScrollLocks();
                                $(".list__item-field").trigger('reset');
                            }
                        },
                    }
                }, {
                    touch: false,
                });



            }
        },
    });

    $('.form-registr').on('click', function (e) {
        e.stopPropagation();
    });

    $("#overlay").on('click', function () {
        $('#overlay').hide();

    });

    document.querySelector(".comment-rating__plus").addEventListener("click", () => {
        var digit = document.querySelector("#span2").innerHTML;
        if (pusk.style.color == 'red') {
            pusk.style.color = '';
            document.querySelector("#span2").innerHTML = digit - 1;
        } else {
            pusk.style.color = 'red';
            document.querySelector("#span2").innerHTML = parseInt(digit) + 1;;
        }
    })

});
